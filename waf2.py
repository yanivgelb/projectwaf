#!/usr/bin/env python
#import urllib
import sys

if sys.version_info[0] == 3:
    from urllib.request import urlopen
else:
    from urllib import urlopen


def get_data():
    html_content = urlopen('https://the-internet.herokuapp.com/context_menu').read()
    html = html_content.decode('ISO-8859-1')
    print(type(html))
    return check_string(html)


def check_string(st):
    print("checking in url if Right-click text exists")
    if "Right-click in the box below to see one called 'the-internet'" in st:
        print("success")
        return True


def test_answer():
        assert get_data() == True


get_data() 
